<?php
	include('template-parts/header.php');
?>

<!-- Page banner -->
<section class="PageBannerSection">
	<div class="BannerOverlay"></div>
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/banner-about.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/aboutbanner.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>About us</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>About</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- About us -->
<section class="Section AboutUsSection greySection">
	<div class="container">
		<div class="LeftImageRightContent">
			<div class="row">
				<div class="col-md-6 MBottom3">
					<div class="TextContainer">
						<div class="MainHeading LeftAlign PBottom22">
							<h2>About Us</h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse mattis felis eu interdum interdum. Aenean et lorem sit amet sapien volutpat porttitor. Fusce ultricies mi</p>
						<p> euismod, pellentesque orci a, consequat mi. Vestibulum egestas sollicitudin purus ut eleifend. Ut lacinia nulla in hendrerit aliquam. Ut ut elit nec urna bibendum sagittis tristique non sem. Sed id dui eget ligula pulvinar ornare sit amet sed nisl. </p>
						<p>Aenean et lorem sit amet sapien volutpat porttitor. Fusce ultricies mi euismod, pellentesque orci a, consequat mi.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ImageContainer">
						<div class="imgWrap per58">
							<img src="assets/img/tempimg/image-ee.png" alt="image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Our History -->
<section class="backgroundImageRightContent BG50per">
	<div class="container">
		<div class="row">
			<div class="col-md-12 backgroundImage-col">
				<div class="backgroundImage">
					<img src="assets/img/tempimg/background-image3.png" alt="">
				</div>
				<div class="Content">
					<h2>Our History</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse mattis felis eu interdum interdum. Aenean et lorem sit amet sapien volutpat porttitor.</p> 
					<p>Fusce ultricies mi euismod, pellentesque orci a, consequat mi. Vestibulum egestas sollicitudin purus ut eleifend. Ut lacinia nulla in hendrerit aliquam. Ut ut elit nec urna bibendum sagittis tristique non sem. Sed id dui eget ligula pulvinar ornare sit amet sed nisl.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	include('template-parts/boardMemberCard.php');
?>

<!-- Our Ethics -->
<section class="Section OurEthicsSection greySection PBottom22">
	<div class="container">
		<div class="LeftImageRightContent">
			<div class="row">
				<div class="col-md-6 MBottom3">
					<div class="TextContainer">
						<div class="MainHeading LeftAlign">
							<h2>Our Ethics:<br> Patient Safety & Quality Care</h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse mattis felis eu interdum interdum. Aenean et lorem sit amet sapien volutpat porttitor. Fusce ultricies mi euismod, pellentesque orci a, consequat mi.</p> 
						<p>Vestibulum egestas sollicitudin purus ut eleifend. Ut lacinia nulla in hendrerit aliquam. Ut ut elit nec urna bibendum sagittis tristique non sem. Sed id dui eget ligula pulvinar ornare sit amet sed nisl.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ImageContainer">
						<div class="imgWrap per58">
							<img src="assets/img/tempimg/our-ethic.png" alt="image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
	include('template-parts/footer.php');
?>
