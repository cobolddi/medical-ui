<?php
	include('template-parts/header.php');
?>

<!-- Page banner -->
<section class="PageBannerSection">
	<div class="BannerOverlay"></div>
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/contactbanner.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/contactbanner.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>Appointments</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>Appointments</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="contactUs greySection">
	<div class="container">
		<div class="formWrapper">
		<div class="headWrap">
			<div class="row">
			  <div class="col-lg-4">
			    <h1>Book Your Appointment Online</h1>
		     </div>
		     <div class="col-lg-8">
				<p>We request you to book your appointment at least 2 days in advance for a consultation.</p>
				<p>To book an appointment call us at <a href="tel:+91-9876543210">+91-9876543210</a> Or fill the form below to submit.</p>
		    </div>
		   </div>
	     </div>
	     <h4>Patient Details</h4>
		<form action="" method="post" class="contactform">
			<div class="row">
				<div class="col-12 col-md-6">
					<input type="text" placeholder="First Name*" name="fName" required>
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Last Name" name="lName">
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Phone Number*" name="Phone" required>
				</div>
				<div class="col-12 col-md-6">
					<input type="email" placeholder="Email Address" name="Email">
				</div>
				<div class="col-12 col-md-6">
					<input placeholder="Date of Birth" type="text" onfocus="(this.type='date')" id="date">
				</div>
				<div class="col-12 col-md-6">
					<input type="number" placeholder="Age" name="age">
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>State</option>
							<option>Delhi</option>
							<option>Haryana</option>
							<option>Punjab</option>
						</select>
				    </div>
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>City</option>
							<option>Faridabad</option>
							<option>New Delhi</option>
							<option>Gurugram</option>
						</select>
				    </div>
				</div>
				<div class="col-12">
					<h4>Doctor Details</h4>
				</div>
				<div class="col-12 col-md-6">
					<input placeholder="Appointment Date" type="text" onfocus="(this.type='date')" id="date">
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Appointment Time" name="time" onfocus="(this.type='time')" id="time">
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>Select Doctor</option>
							<option>Sandeep Sharma</option>
							<option>Nupur Bhardwaj</option>
							<option>Arushi Sharma</option>
							<option>Rakesh Chaudhary</option>
						</select>
				    </div>
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>Select Speciality</option>
							<option>ENT</option>
							<option>Genral Phy</option>
							<option>Dentist</option>
						</select>
				    </div>
				</div>
				<div class="col-12">
					<label for="">Message</label>
					<textarea name="Message" class="MBnone"></textarea>
				</div>
				<div class="col-12">
					<input type="submit" value="Submit" class="Button" id="submit">
				</div>
			</div>
		</form>
		</div>
	</div>
</section>
<?php
	include('template-parts/footer.php');
?>