<?php
	include('template-parts/header.php');
?>

<!-- Page banner -->
<section class="PageBannerSection">
	<div class="BannerOverlay"></div>
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/contactbanner.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/contactbanner.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>Contact Us</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>Contact us</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="contactUs greySection">
	<div class="container">
		<div class="formWrapper">
			<div class="headWrap ContactHead">
				<div class="row">
				  <div class="col-lg-8">
				    <h1>Send us a messages</h1>
				    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius.</p>
			     </div>
			   </div>
		     </div>
			<form action="" method="post" class="contactform">
				<div class="row">
					<div class="col-12 col-md-6">
						<input type="text" placeholder="First Name*" name="fName" required>
					</div>
					<div class="col-12 col-md-6">
						<input type="text" placeholder="Last Name" name="lName">
					</div>
					<div class="col-12 col-md-6">
						<input type="text" placeholder="Phone Number*" name="Phone" required>
					</div>
					<div class="col-12 col-md-6">
						<input type="email" placeholder="Email Address" name="Email">
					</div>
					<div class="col-12">
						<label for="">Message</label>
						<textarea name="Message" class="MBnone"></textarea>
					</div>
					<div class="col-12">
						<input type="submit" value="Submit" class="Button" id="submit">
					</div>
				</div>
			</form>
		</div>
		<div class="ContactDetails">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="GridBlock">
						<h4>South Delhi, India</h4>
						<p>Thinkvalley B13, Sector32, Gurgaon - <br>122002, Haryana (India)</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="GridBlock">
						<h4>Email</h4>
						<p><a href="mailto:hi@cobold.in">hi@cobold.in</a></p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="GridBlock">
						<h4>Phone</h4>
						<p><a href="tel:+91-981079XXXX">+91-981079XXXX</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>





<?php
	include('template-parts/footer.php');
?>