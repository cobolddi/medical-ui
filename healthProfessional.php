<?php
	include('template-parts/header.php');
?>
<!-- <section>
	<div class="banner">
		<img src="assets/img/ourTeamBanner.png" alt="teamBanner">
	</div>
</section> -->

<!-- Page banner -->
<section class="PageBannerSection">
    <div class="BannerOverlay"></div>
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/banner-about.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/healthbanner.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>Health Professionals</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>Health Professionals</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- <section class="searcBarSection emergencySearch">
	<div class="container">
		<div class="searachBarWrapper">
			<h3>Do You Have An Emergency?</h3>
			<form action="" method="">
				<div class="row">
					<div class="col-md-4">
					   <input type="search" name="SearchDoctor" placeholder="Find a Doctor" id="find_doctor">
				    </div>
					<div class="col-md-4">
						<div class="selectWrap">
							<select>
								<option selected disabled>City</option>
								<option>Delhi</option>
								<option>Haryana</option>
								<option>Punjab</option>
							</select>
					    </div>
				    </div>
				    <div class="col-md-4">
				      <input type="search" name="SearchSpeciality" placeholder="Find a Seciality" id="search_speciality">
				    </div>
			    </div>
			    <div class="submit_btn">
			    	<input type="submit" name="submit" id="submitForm" value="">
			    	<img src="assets/img/search_icon.svg" alt="search_icon">
			    </div>
			</form>
		</div>
	</div>
</section> -->
<section class="topImageBottomInfo Section healthProfessional">
	<div class="container">
		<div class="headWrap">
			<h2>Our Health Professionals</h2>
			<p>Highly and accredited health professionals from almost all speciality areas.To provide you with exceptional care. From our family, to yours.</p>
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert1.png" alt="">
                <div class="info">
                    <h3>Sandeep Sharma <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, Deneral Practitioner</span>
                    <span>Working Since 1988</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                        </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert2.png" alt="">
                <div class="info">
                    <h3>Aarushi Sharma <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, Emergency Practitioner</span>
                    <span>Working Since 2002</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                      <svg class="Icon">
                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                      </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert3.png" alt="">
                <div class="info">
                    <h3>Nupur Bharadwaj <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, General Practitioner</span>
                    <span>Working Since 1998</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                      <svg class="Icon">
                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                      </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert4.png" alt="">
                <div class="info">
                    <h3>Rakesh Chaudhary <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, General Practitioner</span>
                    <span>Working Since 2010</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                      <svg class="Icon">
                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                      </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>   
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>

		<div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert1.png" alt="">
                <div class="info">
                    <h3>Sandeep Sharma <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, Deneral Practitioner</span>
                    <span>Working Since 1988</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                        </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert2.png" alt="">
                <div class="info">
                    <h3>Aarushi Sharma <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, Emergency Practitioner</span>
                    <span>Working Since 2002</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                      <svg class="Icon">
                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                      </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert3.png" alt="">
                <div class="info">
                    <h3>Nupur Bharadwaj <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, General Practitioner</span>
                    <span>Working Since 1998</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                      <svg class="Icon">
                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                      </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12 MBottom3">
            <div class="innerContent">
                <img src="assets/img/teamExpert4.png" alt="">
                <div class="info">
                    <h3>Rakesh Chaudhary <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
                    <span>Internist, General Practitioner</span>
                    <span>Working Since 2010</span>
                </div>
                <div class="moreInfo">
                    <span class="shuffleIcon">
                      <svg class="Icon">
                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                      </svg>
                    </span>
                    <div class="socialConnect">
                        <ul>
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>   
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
		</div>
		
	</div>
</section>
<?php
	include('template-parts/footer.php');
?>