<?php
	include('template-parts/header.php');
?>

<?php

    include('template-parts/pageHeader/HomeBanner.php');
	// include('template-parts/searchBarSection.php');
	include('template-parts/LeftImageRightContent.php');
	include('template-parts/topImageBottomInfo.php');
	
?>

<!-- Breakthrough Cases -->
<section class="Section BreakthroughCases">
	<div class="container">
		<div class="MainHeading">
			<h2>Breakthrough Cases</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum.</p>
		</div>
		<div class="LeftImageRightContent PTB3">
			<div class="row">
				<div class="col-md-6 MBottom3">
					<div class="ImageContainer">
						<div class="imgWrap per58">
							<img src="assets/img/tempimg/1232.png" alt="image">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="TextContainer">
						<div class="MainHeading LeftAlign PBottom22">
							<h2>Case study 1</h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor.</p>
					</div>
					<div class="BtnWrap">
						<a href="#" class="Button">
							Read More
							<svg class="Icon">
						        <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
						    </svg>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="LeftImageRightContent PTB3">
			<div class="row">
				<div class="col-md-6 MBottom3">
					<div class="TextContainer">
						<div class="MainHeading LeftAlign PBottom22">
							<h2>Case study 2</h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor.</p>
					</div>
					<div class="BtnWrap">
						<a href="#" class="Button TransParentButton">
							Read More
							<svg class="Icon">
						        <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
						    </svg>
						</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ImageContainer">
						<div class="imgWrap per58">
							<img src="assets/img/tempimg/dr-image1.png" alt="image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	include('template-parts/topImageBottomTextCard.php');
	include('template-parts/testimonial.php');
?>


<?php 
	include('template-parts/footer.php');
?>
	