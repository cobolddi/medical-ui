<?php include('template-parts/header.php'); ?>

<!-- Page banner -->
<section class="PageBannerSection">
	<div class="BannerOverlay"></div>
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/singleservice.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/singleservice.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>Cardiology</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>Cardiology</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Cardiology-->
<section class="topImageBottomInfo Section">
	<div class="container">
		<div class="headWrap">
			<h2>Cardiology</h2>
			<p>At Frontline Health Hospital, we value your life and listen to all your healthcare needs. With our extensive health services in cardiology, we make sure your heart is always running healthy. We at Frontline help you manage existing heart conditions, monitor your heart health and importantly, prevents the onset of heart disease. All this comes to you with our expert’s guidance. </p>
			<strong>Enquire today and we will have you assisted right away.</strong>
		</div>
	<div class="row">
		<div class="col-lg-3 col-sm-6 col-12 OnlyT3">
			<div class="innerContent">
				<img src="assets/img/teamExpert1.png" alt="">
				<div class="info">
					<h3>Sandeep Sharma <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
					<span>Internist, Deneral Practitioner</span>
					<span>Working Since 1988</span>
				</div>
				<div class="moreInfo">
					<span class="shuffleIcon">
						<svg class="Icon">
	                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
	                    </svg>
	                </span>
					<div class="socialConnect">
						<ul>
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                                                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
	                            </svg>
	                        </a>
	                    </li>
	                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6 col-12 OnlyT3">
			<div class="innerContent">
				<img src="assets/img/teamExpert2.png" alt="">
				<div class="info">
					<h3>Aarushi Sharma <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
					<span>Internist, Emergency Practitioner</span>
					<span>Working Since 2002</span>
				</div>
				<div class="moreInfo">
					<span class="shuffleIcon">
					  <svg class="Icon">
	                    <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
	                  </svg>
	                </span>
					<div class="socialConnect">
						<ul>
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                                                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
	                            </svg>
	                        </a>
	                    </li>
	                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6 col-12 OnlyT3">
			<div class="innerContent">
				<img src="assets/img/teamExpert3.png" alt="">
				<div class="info">
					<h3>Nupur Bharadwaj <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
					<span>Internist, General Practitioner</span>
					<span>Working Since 1998</span>
				</div>
				<div class="moreInfo">
					<span class="shuffleIcon">
					  <svg class="Icon">
	                    <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
	                  </svg>
	                </span>
					<div class="socialConnect">
						<ul>
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                                                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
	                            </svg>
	                        </a>
	                    </li>
	                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6 col-12 OnlyT3">
			<div class="innerContent">
				<img src="assets/img/teamExpert4.png" alt="">
				<div class="info">
					<h3>Rakesh Chaudhary <span><img src="assets/img/right_arrow.svg" alt=""></span></h3>
					<span>Internist, General Practitioner</span>
					<span>Working Since 2010</span>
				</div>
				<div class="moreInfo">
					<span class="shuffleIcon">
					  <svg class="Icon">
	                    <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
	                  </svg>
	                </span>
					<div class="socialConnect">
						<ul>
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
	                            </svg>
	                        </a>
	                    </li>	
	                                                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
	                            </svg>
	                        </a>
	                    </li>
	                </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>

   <!-- Breakthrough Cases -->
<section class="Section BreakthroughCases greySection">
	<div class="container">
		<div class="MainHeading">
			<h2>Breakthrough Cases</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum.</p>
		</div>
		<div class="LeftImageRightContent PTB3">
			<div class="row">
				<div class="col-md-6 MBottom3">
					<div class="ImageContainer">
						<div class="imgWrap per58">
							<img src="assets/img/newborn-heart-surgery.png" alt="image">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="TextContainer">
						<div class="MainHeading LeftAlign PBottom22">
							<h2>Saving A Newborn's Heart With Reliable Surgical procedures:</h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="LeftImageRightContent PTB3">
			<div class="row">
				<div class="col-md-6 MBottom3">
					<div class="TextContainer">
						<div class="MainHeading LeftAlign PBottom22">
							<h2>Advance In Heart Transplant Surgey:</h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ImageContainer">
						<div class="imgWrap per58">
							<img src="assets/img/heart-transplant-surgery.png" alt="image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Patient Testimonial -->
<section class="testimonialSection Section">
    <div class="container">
        <div class="testimonialWrapper">
            <div class="row">
                <div class="col-md-8 order-2-md">
                  <div class="testimonialSlider">
                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                  </div>
                </div>
                <div class="col-md-4 alignSelfCenter">
                  <h2>Patient Testimonials</h2>
                </div>
            </div>
             <div class="slick-controls">
              <span class="next">
                <svg>
                  <use xlink:href="assets/img/cobold-sprite.svg#icon-arrow"></use>
                </svg>
              </span>
              <span class="previous">
                <svg>
                  <use xlink:href="assets/img/cobold-sprite.svg#icon-arrow"></use>
                </svg>
              </span>
            </div>
        </div>
    </div>
</section>

<!-- Book your Appointment-->
<section class="contactUs greySection Section">
	<div class="container">
		<div class="formWrapper MTop0">
		<div class="headWrap">
			<div class="row">
			  <div class="col-lg-4">
			    <h1>Book Your Appointment Online</h1>
		     </div>
		     <div class="col-lg-8">
				<p>We request you to book your appointment at least 2 days in advance for a consultation.</p>
				<p>To book an appointment call us at <a href="tel:+91-9876543210">+91-9876543210</a> Or fill the form below to submit.</p>
		    </div>
		   </div>
	     </div>
	     <h4>Patient Details</h4>
		<form action="" method="post" class="contactform">
			<div class="row">
				<div class="col-12 col-md-6">
					<input type="text" placeholder="First Name*" name="fName" required>
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Last Name" name="lName">
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Phone Number*" name="Phone" required>
				</div>
				<div class="col-12 col-md-6">
					<input type="email" placeholder="Email Address" name="Email">
				</div>
				<div class="col-12 col-md-6">
					<input placeholder="Date of Birth" type="text" onfocus="(this.type='date')" id="date">
				</div>
				<div class="col-12 col-md-6">
					<input type="number" placeholder="Age" name="age">
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>State</option>
							<option>Delhi</option>
							<option>Haryana</option>
							<option>Punjab</option>
						</select>
				    </div>
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>City</option>
							<option>Faridabad</option>
							<option>New Delhi</option>
							<option>Gurugram</option>
						</select>
				    </div>
				</div>
				<div class="col-12">
					<h4>Doctor Details</h4>
				</div>
				<div class="col-12 col-md-6">
					<input placeholder="Appointment Date" type="text" onfocus="(this.type='date')" id="date">
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Appointment Time" name="time" onfocus="(this.type='time')" id="time">
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>Select Doctor</option>
							<option>Sandeep Sharma</option>
							<option>Nupur Bhardwaj</option>
							<option>Arushi Sharma</option>
							<option>Rakesh Chaudhary</option>
						</select>
				    </div>
				</div>
				<div class="col-12 col-md-6">
					<div class="selectWrap">
						<select>
							<option selected disabled>Select Speciality</option>
							<option>ENT</option>
							<option>Genral Phy</option>
							<option>Dentist</option>
						</select>
				    </div>
				</div>
				<div class="col-12">
					<textarea placeholder="Message" name="Message" class="MBnone"></textarea>
				</div>
				<div class="col-12">
					<input type="submit" value="Submit" class="Button" id="submit">
				</div>
			</div>
		</form>
		</div>
	</div>
</section>
</div>
<?php include('template-parts/footer.php'); ?>