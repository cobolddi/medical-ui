jQuery(function($){
  'use strict'
  $(document).ready(function() {
    let ES6 = x => console.log("ES6 Running");
    ES6();
    let Page_Height = $(document).height();
    console.log("Page_Height", Page_Height);
    let WindowWidth = $( window ).width();
    console.log("window_width", WindowWidth);


    var headerHeight = $('#header').outerHeight(true);
    console.log("Header Height", headerHeight);
    $('main').css('paddingTop', headerHeight);

    //Mobile menu
    $(".MenuBar").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").addClass("Active_Nav");
    });
    $(".MenuOverlay").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").removeClass("Active_Nav");
    });

    //Check Screen size
    let checkScrennSize = () => {
      if (window.matchMedia('(max-width: 991px)').matches) {
          // $(".haveSubmenu>a").append('<span><img src="assets/img/chevron-down-white.svg"></span>');
          $(".haveSubmenu>a").append('<span></span>');
      }
      $(".haveSubmenu").find('a>span').click(function(e){
      // $(".haveSubmenu>a>span").click(function(e){
        // e.stopPropagation();
        e.preventDefault();
        $(this).parents('a').parent().toggleClass("Active_MobileDropdown");
      });
    }
    checkScrennSize();

    //Take out popup form
    $('.takeout').magnificPopup({
      type: 'inline',
      mainClass: 'mfp-fade',
      removalDelay: 100,
      preloader: true,
      fixedContentPos: false
  });

    //Gallery Slider
    $(".GallerySlider").slick({
      dots: false,
      arrow: true,
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    });

    //Card Slider
    $(".CardSlider").slick({
      dots: false,
      arrow: true,
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
    });
    
    // init Isotope
    var $grid = $('.grid').isotope({
      itemSelector: '.element-item',
      layoutMode: 'fitRows'
    });
    // filter functions
    var filterFns = {
      // show if number is greater than 50
      numberGreaterThan50: function() {
        var number = $(this).find('.number').text();
        return parseInt( number, 10 ) > 50;
      },
      // show if name ends with -ium
      ium: function() {
        var name = $(this).find('.name').text();
        return name.match( /ium$/ );
      }
    };
    // bind filter button click
    $('.filters-button-group').on( 'click', 'button', function() {
      var filterValue = $( this ).attr('data-filter');
      // use filterFn if matches value
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
    });


    $(window).resize(function () {
      if ($(window).width() <= 991) {
        var cardslider = $(".cardSliderSection .container-fluid");
        cardslider.removeClass("container-fluid").addClass("container");
      }
    }); 
    // HomePage Banner Slider
    $(".bannerSlider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrow: false,
      fade: true,
      autoplay: true,
      pauseOnHover: false,
      speed: 800,
      infinite: true,
      cssEase: 'ease-in-out',
      touchThreshold: 100
    }); 

    // Patient Testimonial Slider
    $(".testimonialSlider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrow: false,
      // fade: true,
      autoplay: true,
      nextArrow: '.next',
      prevArrow: '.previous',
      pauseOnHover: false,
      speed: 800,
      infinite: true,
      cssEase: 'ease-in-out',
      touchThreshold: 100
    }); 


    $(".moreInfo").on("click", function(){
      var social = $(".moreInfo .socialConnect");
      var shuffleIcon = $('.moreInfo .shuffleIcon');
      $(this).find(shuffleIcon).toggleClass("is_active");
      $(this).find(social).fadeToggle();
    });
    
  }); //--End document.ready();
});

  