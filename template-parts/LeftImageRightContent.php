<section class="Section LeftImageRightContent">
	<div class="container">
		<div class="row">
			<div class="col-md-6 MBottom2">
				<div class="ImageContainer">
					<div class="imgWrap">
						<img src="assets/img/tempimg/O6RBX-kH0.png" alt="image">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="TextContainer">
					<div class="MainHeading LeftAlign PBottom22">
						<h2>A Wealth Of Experience To Heal And Help You</h2>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod nisi sit amet tellus vulputate, suscipit egestas urna varius. Donec nec purus magna. Donec interdum elementum erat et condimentum. Praesent arcu orci, blandit non fermentum vitae, vestibulum sit amet tortor. Vivamus convallis et mi et varius. Ut tincidunt diam et mattis consequat. Sed faucibus risus eget nisi tempor malesuada. Aliquam erat volutpat. Sed luctus neque ac varius tempus. Aliquam nec pretium mi. Phasellus in placerat dui. Phasellus posuere purus sed odio pretium fringilla. Maecenas sodales laoreet lorem, non maximus metus rutrum non. Fusce id laoreet turpis.</p>
				</div>
				<div class="BtnWrap">
					<a href="#" class="Button TransParentButton">
						Read More
						<svg class="Icon">
					        <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
					    </svg>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>