<section class="backgroundImageRightContent BG50per">
	<div class="container">
		<div class="row">
			<div class="col-md-12 backgroundImage-col">
				<div class="backgroundImage">
					<img src="assets/img/tempimg/background-image3.png" alt="">
				</div>
				<div class="Content">
					<h2>Our History</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse mattis felis eu interdum interdum. Aenean et lorem sit amet sapien volutpat porttitor.</p> 
					<p>Fusce ultricies mi euismod, pellentesque orci a, consequat mi. Vestibulum egestas sollicitudin purus ut eleifend. Ut lacinia nulla in hendrerit aliquam. Ut ut elit nec urna bibendum sagittis tristique non sem. Sed id dui eget ligula pulvinar ornare sit amet sed nisl.</p>
				</div>
			</div>
		</div>
	</div>
</section>