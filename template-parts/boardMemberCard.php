<section class="Section boardMemberCardSection">
	<div class="container">
		<div class="MainHeading">
			<div class="Heading">
				<h2>Board Members</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 MBottom3">
				<div class="boardMemberCard">
					<div class="Cardimage">
						<img src="assets/img/tempimg/member-1.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Dr. Sandeep Sharma</h3>
						<span class="memberInfo">Internist, General Practitioner</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 MBottom3">
				<div class="boardMemberCard">
					<div class="Cardimage">
						<img src="assets/img/tempimg/member-2.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Dr. Nitin Chauhan</h3>
						<span class="memberInfo">Internist Emergency Physician</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 MBottom3">
				<div class="boardMemberCard">
					<div class="Cardimage">
						<img src="assets/img/tempimg/member-1.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Dr. Sandeep Sharma</h3>
						<span class="memberInfo">Internist, General Practitioner</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 MBottom3">
				<div class="boardMemberCard">
					<div class="Cardimage">
						<img src="assets/img/tempimg/member-3.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Rakesh Chaudhri</h3>
						<span class="memberInfo">Internist, General Practitioner</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>