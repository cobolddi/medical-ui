<section class="Section">
	<div class="container">
		<div class="MainHeading">
			<div class="Heading">
				<h2>Medical Specialities & Treatments</h2>
				<p>Our specialised and focused healthcare services to give you the best assistance in your recovery.</p>
			</div>
		</div>
	</div>
</section>
<section class="Section cardSliderSection greySection">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2 offset-2">
				<h2 class="SliderHead">Clinical Specialities</h2>
			</div>
			<div class="col-lg-8 CardSlider-Col">
				<div class="CardSlider">
					<div class="boardMemberCardWrap">
						<div class="boardMemberCard">
							<div class="Cardimage">
								<img src="assets/img/tempimg/paediatrician.png" alt="member-1">
							</div>
							<div class="CardText TextCenter">
								<h3 class="memberName">paediatrician</h3>
							</div>
						</div>
					</div>
					<div class="boardMemberCardWrap">
						<div class="boardMemberCard">
							<div class="Cardimage">
								<img src="assets/img/tempimg/gynaecology.png" alt="member-1">
							</div>
							<div class="CardText TextCenter">
								<h3 class="memberName">Gynaecology</h3>
							</div>
						</div>
					</div>
					<div class="boardMemberCardWrap">
						<div class="boardMemberCard">
							<div class="Cardimage">
								<img src="assets/img/tempimg/paediatrician.png" alt="member-1">
							</div>
							<div class="CardText TextCenter">
								<h3 class="memberName">Chemotherapy</h3>
							</div>
						</div>
					</div>
					<div class="boardMemberCardWrap">
						<div class="boardMemberCard">
							<div class="Cardimage">
								<img src="assets/img/tempimg/gynaecology.png" alt="member-1">
							</div>
							<div class="CardText TextCenter">
								<h3 class="memberName">Dialysis Clinics</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>