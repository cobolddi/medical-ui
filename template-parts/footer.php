
    </main>
    <?php include('template-parts/BookAppointment.php'); ?>
    <footer>
        <div class="container">
            <div class="subscribeNewsletter">
                <div class="row alignItemCenter">
                    <div class="col-md-7">
                        <h2>Subscribe To Our Weekly Newsletter</h2>
                    </div>
                    <div class="col-md-5">
                        <form action="">
                            <div class="input-group">
                            <input type="email" name="EmailAddress" placeholder="Enter your Email">
                             <div class="btn_wrap">
                                <input type="submit" name="submit" value="">
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row MBottom2">
                <div class="col-lg-3 col-md-12 p-r-5">
                    <div class="footer_logo">
                        <a href="index.php">
                            <img src="assets/img/footer-logo.png" data-retina="true" alt="companyLogo">
                        </a>  
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris scelerisque ante gravida nibh elementum</p>                                      
                </div>
                <div class="col-lg-2 col-6">
                    <h5 class="FooterHeadingWithBorder">Services Link</h5>
                    <ul class="links">
                        <li><a href="about-us.php">About Us</a></li>
                        <li><a href="treatments.php">Treatments</a></li>
                        <li><a href="healthProfessional.php">Health Professionals</a></li>
                    </ul>
                </div>    
                <div class="col-lg-2 col-6">
                    <h5 class="FooterHeadingWithBorder">Quick Link</h5>
                    <ul class="links">                        
                        <li><a href="appointments.php">Appointments</a></li>
                        <li><a href="contact-us.php">Contact us</a></li>
                    </ul>
                </div> 
                <div class="col-lg-3 col-6">
                    <h5 class="FooterHeadingWithBorder">Stay In Touch</h5>
                    <ul class="contacts">
                        <li>
                            <span class="ContentWrap">
                                <a href="tel:+919876543210"> +91-9876543210</a>
                            </span>
                        </li>
                        <li>
                            <span class="ContentWrap">
                                <a href="mailto:info@medicalsolutions.com">info@medicalsolutions.com</a>
                            </span>
                        </li>
                        <li>
                            <span class="ContentWrap">
                                Farm No. 4, Second Floor, <br>Club Drive, Ghitorni - 110030
                            </span>
                            
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-6">
                    <h5 class="FooterHeadingWithBorder">Follow Us</h5>
                    
                   <!--  <p>Tuesday - Thursday | 6:00 pm - 11:00 pm</p>
                    <p> Friday - Saturday | 6:00 pm - 12:00 am</p> -->
                    <div class="follow_us">
                        <ul class="SocialIcons">
                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
                                    </svg>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                    </svg>
                                </a>
                            </li>
                            
                            <li>
                                <a href="https://www.linkedin.com/company/max-asset-services-by-maxgroup/?viewAsMember=true" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                    </svg>
                                </a>
                            </li>
                                                            
                            <li>
                                <a href="https://twitter.com/asset_max" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <!--/row-->
        </div>
        <div class="FooterBottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <ul id="footer-selector">
                            <li>Copyright &copy; <script>document.write(new Date().getFullYear())</script> Restaurant. All rights reserved.</li>
                        </ul>
                    </div>
                    <!-- <div class="col-lg-6">
                        <ul id="additional_links">
                            <li><a href="http://cobold.co/" target="_blank">Delivered by  <strong>Cobold Digital</strong></a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
</div>
<!-- page -->
      
    <div id="toTop"></div><!-- Back to top button -->
    <script src="assets/js/vendor.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>
</html>