<section class="formSection Section">
	<div class="container">
		<div class="headWrap">
			<h2>Book A Table</h2>
			<p>To book a table,call us at <a href="tel:+919876543210">+919876543210</a> or use the form below.</p>
		</div>
		<form action="" method="post" class="contactform">
			<div class="row">
				<div class="col-12 col-md-6">
					<input type="text" placeholder="First Name*" name="fName" required>
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Last Name" name="lName">
				</div>
				<div class="col-12 col-md-6">
					<input type="email" placeholder="Email" name="Email">
				</div>
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Phone Number*" name="Phone" required>
				</div>
				<div class="col-12">
					<textarea placeholder="Message" name="Message" class="MBnone"></textarea>
				</div>
				<div class="col-12 text-right">
					<input type="submit" value="Submit" class="Button" id="submit">
				</div>
			</div>
		</form>
	</div>
</section>