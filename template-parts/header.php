<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Medical</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Medical Solutions">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
</head>
<body>
	<header class="Header" id="header">
		<div class="container">
			<div class="MenuContainer">
				<div class="LogoContainer">
					<a href="index.php"><img src="assets/img/header-logo.png"></a>
				</div>
				<div class="NavContainer">
					<nav>
						<div class="menu-main-menu-container">
							<ul>
								<li><a href="about-us.php">About us</a></li>

								<li><a href="treatments.php">Treatments</a></li>
								<li><a href="healthProfessional.php">Health Professionals</a></li>
								<li><a href="contact-us.php">Contact us</a></li>
								<li class="MenuButton"><a href="appointments.php">Appointments</a></li>
							</ul>
						</div>
					</nav>
					<div class="MenuOverlay MobileNavController">
						<a class="CloseMenuIcon MobileNavController" href="#">
							<span>&times;</span>
						</a>
					</div>
					<a class="MenuBar MobileNavController" href="#">
						<div>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</header>
	<main>
	

	