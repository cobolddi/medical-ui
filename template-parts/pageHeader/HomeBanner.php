<section class="HomeBanner">
    <div class="bannerSlider">
        <div class="bannerWrapper">
            <div class="bannerImage">
                <picture>
                    <source media="(min-width: 768px)" srcset="assets/img/banner.png" >
                    <img src="assets/img/mobileBanner.png" alt="banner_image">
                </picture>
            </div>
            <div class="bannerDetailWrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="bannerDetails">
                                <h1>Welcome to the Frontline Health Hospital</h1>
                                <a href="#" class="Button">
                                	Contact Us
									<svg class="Icon">
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>

       <div class="bannerWrapper">
            <div class="bannerImage">
                <picture>
                    <source media="(min-width: 768px)" srcset="assets/img/banner.png" >
                    <img src="assets/img/mobileBanner.png" alt="banner_image">
                </picture>
            </div>
            <div class="bannerDetailWrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="bannerDetails">
                                <h1>Welcome to the Frontline Health Care</h1>
                                <a href="#" class="Button">
                                	Contact Us
									<svg class="Icon">
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
                                    </svg>
                            	</a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="bannerWrapper">
            <div class="bannerImage">
                <picture>
                    <source media="(min-width: 768px)" srcset="assets/img/banner.png" >
                    <img src="assets/img/mobileBanner.png" alt="banner_image">
                </picture>
            </div>
            <div class="bannerDetailWrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="bannerDetails">
                                <h1>Welcome to the Frontline Health Support</h1>
                                <a href="#" class="Button">
                                	Contact Us
                                	<svg class="Icon">
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>