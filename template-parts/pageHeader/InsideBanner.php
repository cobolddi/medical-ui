<section class="PageBannerSection">
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/banner-about.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/banner-about.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>About us</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>About</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>