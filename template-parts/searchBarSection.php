<section class="searcBarSection">
	<div class="container">
		<div class="searachBarWrapper">
			<h3>Do You Have An Emergency?</h3>
			<form action="" method="">
				<div class="row">
					<div class="col-md-4">
					   <input type="search" name="SearchDoctor" placeholder="Find a Doctor" id="find_doctor">
				    </div>
					<div class="col-md-4">
						<div class="selectWrap">
							<select required>
								<option value="" selected disabled>City</option>
								<option value="Delhi">Delhi</option>
								<option value="Haryana">Haryana</option>
								<option value="Punjab">Punjab</option>
							</select>
					    </div>
				    </div>
				    <div class="col-md-4">
				      <input type="search" name="SearchSpeciality" placeholder="Find a Seciality" id="search_speciality">
				    </div>
			    </div>
			    <div class="submit_btn">
			    	<input type="submit" name="submit" id="submitForm" value="">
			    	<img src="assets/img/search_icon.svg" alt="search_icon">
			    </div>
			</form>
		</div>
	</div>
</section>