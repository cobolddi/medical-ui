<section class="testimonialSection">
    <div class="container">
        <div class="testimonialWrapper">
            <div class="row">
                <div class="col-md-8 order-2-md">
                  <div class="testimonialSlider">
                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                    <div class="innnerSlider">
                      <div class="patient_info">
                          <img src="assets/img/patient_img.png" alt="">
                          <div class="name">
                            <h3>Uday Honda</h3>
                            <small>Businessman</small>
                          </div>
                      </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>

                  </div>
                </div>
                <div class="col-md-4 alignSelfCenter">
                  <h2>Patient Testimonials</h2>
                </div>
            </div>
             <div class="slick-controls">
              <span class="next">
                <svg>
                  <use xlink:href="assets/img/cobold-sprite.svg#icon-arrow"></use>
                </svg>
              </span>
              <span class="previous">
                <svg>
                  <use xlink:href="assets/img/cobold-sprite.svg#icon-arrow"></use>
                </svg>
              </span>
            </div>
        </div>
    </div>
</section>