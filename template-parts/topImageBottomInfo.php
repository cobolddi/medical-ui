<section class="topImageBottomInfo greySection Section">
	<div class="container">
		<div class="headWrap">
			<h2>Our Teams Of Experts</h2>
			<!-- <div class="MobileOnly">
				<a href="#" class="Button">
					View More 
					<svg class="Icon">
	                    <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
	                </svg>
	            </a>
            </div> -->
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-6 col-12 MBottom3">
				<div class="innerContent">
					<img src="assets/img/teamExpert1.png" alt="">
					<div class="info">
						<h3>Sandeep Sharma</h3>
						<span>Internist, Deneral Practitioner</span>
						<span>Working Since 1988</span>
					</div>
					<div class="moreInfo">
						<span class="shuffleIcon">
							<svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                            </svg>
                        </span>
						<div class="socialConnect">
							<ul>
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                    </svg>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                    </svg>
                                </a>
                            </li>
                                                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-12 MBottom3">
				<div class="innerContent">
					<img src="assets/img/teamExpert2.png" alt="">
					<div class="info">
						<h3>Aarushi Sharma</h3>
						<span>Internist, Emergency Practitioner</span>
						<span>Working Since 2002</span>
					</div>
					<div class="moreInfo">
						<span class="shuffleIcon">
						  <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                          </svg>
	                    </span>
						<div class="socialConnect">
							<ul>
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                    </svg>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                    </svg>
                                </a>
                            </li>
                                                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-12 MBottom3">
				<div class="innerContent">
					<img src="assets/img/teamExpert3.png" alt="">
					<div class="info">
						<h3>Nupur Bharadwaj</h3>
						<span>Internist, General Practitioner</span>
						<span>Working Since 1998</span>
					</div>
					<div class="moreInfo">
						<span class="shuffleIcon">
						  <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                          </svg>
	                    </span>
						<div class="socialConnect">
							<ul>
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                    </svg>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                    </svg>
                                </a>
                            </li>
                                                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-12 MBottom3">
				<div class="innerContent">
					<img src="assets/img/teamExpert4.png" alt="">
					<div class="info">
						<h3>Rakesh Chaudhary</h3>
						<span>Internist, General Practitioner</span>
						<span>Working Since 2010</span>
					</div>
					<div class="moreInfo">
						<span class="shuffleIcon">
						  <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#icon-plusBold"></use>
                          </svg>
	                    </span>
						<div class="socialConnect">
							<ul>
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                    </svg>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                    </svg>
                                </a>
                            </li>	
                                                            
                            <li>
                                <a href="#" target="_blank">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<a href="#" class="Button">
				View More 
				<svg class="Icon">
                    <use xlink:href="assets/img/cobold-sprite.svg#icon-plus"></use>
                </svg>
            </a>
        </div>
	</div>
</section>