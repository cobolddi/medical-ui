<section class="Section TopImageBottomTextCard greySection">
	<div class="container">
		<div class="MainHeading">
			<h2>Leading The First Line of Defence In Health Service</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget vehicula maximus, massa turpis vestibulum sapien, sed blandit nisi magna eu lacus.</p>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="#" class="cardContainer_col">
					<div class="cardContainer">
						<div class="cardImage">
							<img src="assets/img/csr1.png" alt="">
						</div>
						<div class="cardText">
							<h3 class="cardTitle">CSR 1</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget.</p>
							<a href="#" class="knowMore_link"><span>Know More</span>
							<svg class="icon icon-chevron-right"><use xlink:href="assets/img/cobold-sprite.svg#icon-chevron-right"></use></svg></a>
						</div>
					</div>
				</a>
		   </div>
		   <div class="col-md-4">
				<a href="#" class="cardContainer_col">
					<div class="cardContainer">
						<div class="cardImage">
							<img src="assets/img/csr2.png" alt="">
						</div>
						<div class="cardText">
							<h3 class="cardTitle">CSR 2</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
							<a href="#" class="knowMore_link"><span>Know More</span>
							<svg class="icon icon-chevron-right"><use xlink:href="assets/img/cobold-sprite.svg#icon-chevron-right"></use></svg></a>
						</div>
					</div>
				</a>
		    </div>
		    <div class="col-md-4">
				<a href="#" class="cardContainer_col">
					<div class="cardContainer">
						<div class="cardImage">
							<img src="assets/img/csr3.png" alt="">
						</div>
						<div class="cardText">
							<h3 class="cardTitle">CSR 3</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit, urna eget.</p>
							<a href="#" class="knowMore_link"><span>Know More</span>
							<svg class="icon icon-chevron-right"><use xlink:href="assets/img/cobold-sprite.svg#icon-chevron-right"></use></svg></a>
						</div>
					</div>
				</a>
		    </div>
		</div>
		
	</div>
</section>