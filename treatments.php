<?php
	include('template-parts/header.php');
?>

<!-- Page banner -->
<section class="PageBannerSection">
	<div class="BannerOverlay"></div>
  <div class="BannerContainer">
    <div class="BannerOverly"></div>
    <picture class="DesktopBanner">
      <source srcset="assets/img/tempimg/banner-about.png" media="(max-width: 767px)">
      <img src="assets/img/tempimg/treatmentbanner.png" alt="Banner">
    </picture>
    <img src="assets/img/tempimg/500x360.png" ail="Banner" class="MobileBanner">
    <div class="BannerTextConainer">
      <div class="container">
        <div class="BannerText pl30">
          <h1>Treatments</h1>
          <p>
            <span>
              <a href="#" class="linkColor">Home</a> / <span>Treatments</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<?php 
	include('template-parts/cardSlider.php');
 ?>

<!-- Services -->
<section class="Section boardMemberCardSection ">
	<div class="container">
		<div class="MainHeading">
			<div class="Heading">
				<h2>Services</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 MBottom3">
				<div class="boardMemberCard withShadow">
					<div class="Cardimage">
						<img src="assets/img/tempimg/treatments-1.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">24x7 Emergency Trauma Services</h3>
						<!-- <span class="memberInfo">Internist, General Practitioner</span> -->
					</div>
				</div>
			</div>
			<div class="col-md-4 MBottom3">
				<div class="boardMemberCard withShadow">
					<div class="Cardimage">
						<img src="assets/img/tempimg/treatments-2.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Rheumatology and Arthritis Clinic</h3>
						<!-- <span class="memberInfo">Internist Emergency Physician</span> -->
					</div>
				</div>
			</div>
			<div class="col-md-4 MBottom3">
				<div class="boardMemberCard withShadow">
					<div class="Cardimage">
						<img src="assets/img/tempimg/treatments-3.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Chemotherapy and Dialysis Clinics</h3>
						<!-- <span class="memberInfo">Internist, General Practitioner</span> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Support Services -->
<section class="Section boardMemberCardSection greySection">
	<div class="container">
		<div class="MainHeading">
			<div class="Heading">
				<h2>Support Services</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 MTop3">
				<div class="boardMemberCard withShadow">
					<div class="Cardimage">
						<img src="assets/img/tempimg/support-services-1.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Intensive Care</h3>
						<!-- <span class="memberInfo">Internist, General Practitioner</span> -->
					</div>
				</div>
			</div>
			<div class="col-md-4 MTop3">
				<div class="boardMemberCard withShadow">
					<div class="Cardimage">
						<img src="assets/img/tempimg/support-services-2.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Pain Management</h3>
						<!-- <span class="memberInfo">Internist Emergency Physician</span> -->
					</div>
				</div>
			</div>
			<div class="col-md-4 MTop3">
				<div class="boardMemberCard withShadow">
					<div class="Cardimage">
						<img src="assets/img/tempimg/support-services-3.png" alt="member-1">
					</div>
					<div class="CardText TextCenter">
						<h3 class="memberName">Physiotherapy and Rehabilitation</h3>
						<!-- <span class="memberInfo">Internist, General Practitioner</span> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
	include('template-parts/footer.php');
?>
